import json
from os import path
from django.db import models
from django.conf import settings
from django.urls import reverse
from django.contrib.gis import forms
from django.contrib.gis.db import models as geo_models
from django.contrib.gis.geos import Polygon


# Create your models here


class Site(models.Model):
    title = models.CharField(max_length=150)
    polygon = geo_models.PolygonField()

    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse('sites', kwargs={'pk': self.pk})


def populate_sites():
    with open('sites.json') as json_file:
        data = json.load(json_file)

    for site in data:
        # print(site['title'])
        # print(len(site['points']))
        p_list = [tuple(p) for p in site['points']]
        points = tuple(p_list)
        p = Polygon(points)
        site_obj = {'title': site['title'], 'polygon': p}
        # print(poly)
        s = Site(**site_obj)
        s.save()
    print(f'{len(data)} sites imported')


class Hazard(models.Model):
    title = models.CharField(max_length=150)
    point = geo_models.PointField(srid=4326, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    sites = models.ManyToManyField(to=Site, related_name='contains')

    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse('hazards', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):

        super(Hazard, self).save(*args, **kwargs)
        if self.pk:
            aff_sites = self.detect_sites()
            # print(aff_sites.count())
            self.sites.set(aff_sites)

    def detect_sites(self):
        hit_site_ids = []
        for s in Site.objects.all():
            if self.point.intersects(s.polygon):
                hit_site_ids.append(s.pk)
                print(s.pk, s, 'is hit')
        sites = Site.objects.filter(pk__in=hit_site_ids)

        print(f'Hazard {self} affects {sites.count()} site(s)')
        return sites
