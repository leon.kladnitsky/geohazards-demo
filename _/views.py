from django.contrib.gis import forms
from django.http import HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import ListView, DetailView
from .models import Hazard, Site
from .forms import HazardForm, HazardModelForm, HazardCoordForm


# Create your views here.


class SiteList(ListView):
    model = Site


class SiteDetails(DetailView):
    model = Site


class HazardList(ListView):
    model = Hazard


class HazardDetails(DetailView):
    model = Hazard


def add_hazard_by_coords(request):
    print(request)
    if request.method == 'POST':
        form = HazardCoordForm(request.POST)
        if form.is_valid():
            cleaned = form.cleaned_data
            print('cleaned: ', cleaned)
            h = Hazard(cleaned.get(0))
            print('hazard: ', h)
            h.save()
        return HazardList.as_view()

    form_class = HazardCoordForm()
    return render(request, 'add_hazard.html', {
        'form': form_class,
        'widget': forms.OSMWidget({'map_width': 500, 'map_height': 500})
    })


def add_hazard_by_widget(request):
    print(request)
    if request.method == 'POST':
        form = HazardForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            print('data: ', data)
            h = Hazard(data.get(0))
            print('hazard: ', h)
            h.save()
        return HazardList.as_view()

    form_class = HazardModelForm()
    return render(request, 'new_hazard.html', {
        'form': form_class,
        'widget': forms.OSMWidget({'default_lon': 35, 'default_lat': 32, 'map_width': 500, 'map_height': 500})
    })
