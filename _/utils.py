import json
from django.contrib.gis.geos import Polygon
from hazards.models import Site
# from django.


def populate_sites():
    with open('sites.json') as json_file:
        data = json.load(json_file)
    print(len(data))
    sites = []
    for site in data:
        # print(site['title'])
        # print(len(site['points']))
        p_list = [tuple(p) for p in site['points']]
        points = tuple(p_list)
        p = Polygon(points)
        site_obj = {'title': site['title'], 'polygon': p}
        # print(poly)
        s = Site(**site_obj)
        s.save()

        # sites.append({'title': site['title'], 'polygon': site['points']})
        # poly = Polygon(((0.0, 0.0), (0.0, 50.0), (50.0, 50.0), (50.0, 0.0), (0.0, 0.0)) )



