from django import forms
from django.contrib.gis import forms as geo_forms
from .models import Hazard

DEF_POINT_ATTRS = {'map_width': 500, 'map_height': 500, 'default_lat': 32, 'default_lon': 35}


class HazardForm(forms.Form):

    class Meta:
        title = forms.CharField(max_length=150)
        # point = geo_forms.PointField(widget=forms.OSMWidget(attrs=DEF_POINT_ATTRS))


class HazardModelForm(forms.ModelForm):

    title = forms.CharField(max_length=150)
    # point = geo_forms.PointField(widget=forms.OSMWidget(attrs=DEF_POINT_ATTRS))

    class Meta:
        model = Hazard
        fields = ['title', 'point']
        widgets = {
            'point': geo_forms.OSMWidget(attrs=DEF_POINT_ATTRS)
        }


class HazardCoordForm(forms.Form):
    title = forms.CharField(max_length=150)
    h_lat = forms.FloatField(max_value=180.0, min_value=-180.0)
    h_lng = forms.FloatField(max_value=180.0, min_value=-180.0)
