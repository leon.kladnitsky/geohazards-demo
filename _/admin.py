from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin
from .models import Hazard, Site
# Register your models here.


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    pass


@admin.register(Hazard)
class HazardAdmin(OSMGeoAdmin):
    exclude = ('sites',)
    pass
